<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 16/07/18
 * Time: 09:36 PM
 */

// Envio de sms
function sms($datos){
//    echo (' Ingreso SMS - ');
    $nombre = stristr($datos['nombres'], ' ',true);
    $numero = $datos['celular'];
    $url = 'https://api.hablame.co/sms/envio/';
    $data = array(
        'cliente' => 10010490,
        'api' => 'MjodwcFa6hH9Mj66cLiyhDeXMbaiXB',
        'numero' => '57'.$numero,
        'sms' => 'Felicitaciones '.ucfirst ($nombre).'! Hoy das el primer paso para hacerte profesional con la Uvirtual. Uno de nuestros asesores te contactara para brindarte mayor información.',
        'fecha' => '',
        'referencia' => 'Uvirtual',
    );

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );

    $context  = stream_context_create($options);
    $result = json_decode((file_get_contents($url, false, $context)), true);

    $resSMS = $result['sms']['1'];


    if ($resSMS['resultado']==='0'){
        print ' Se ha enviado el SMS ';
        print_r($resSMS['resultado']);
    }
    else{
        print ' ha ocurrido un error!! ';
        print_r ($resSMS['resultado_t']);
    }

    return $resSMS;
}

// Funcion externo del programa vs CRM
function CodigoExt($programa){
    $programas = array(
        'ADMINISTRACION DE EMPRESAS'					=>	'104413',
        'PUBLICIDAD' 									=> 	'103494',
        'TECNICO PROFESIONAL EN PROCESOS CONTABLES' 	=> '104627',
        'TECNOLOGIA EN GESTION CONTABLE'	 			=> 	'104421',
        'CONTADURIA PUBLICA' 							=> 	'104462',
        'TECNICO PROFESIONAL EN OPERACIÓN DE OBJETOS VIRTUALES'		=>	'102956',
        'TECNOLOGIA EN GESTION DE LA COMUNICACIÓN DIGITAL' 			=> 	'102955',
        'DISEÑO GRAFICO' 								=> 	'102954',
        'SEMINARIO FUNDAMENTOS DE GRAFOLOGÍA FORENSE' 	=> 	'5',
        'SEMINARIO LA PRUEBA PERICIAL EN EL CODIGO DE PROCEDIMIENTO PENAL' 	=> 	'6',
        'SEMINARIO HABEAS DATA Y LEY DE PROTECCIÓN DE DATOS PERSONALES'		=> 	'7',
        'CURSO NIVEL DE INGLES' 						=> 	'8',
        'DIPLOMADO HSEQ' 								=> 	'2017GR1'
    );

    foreach ($programas as $key => $value) {
        # code...
        if ($programa == $key) {
            # code...
            //echo ('resultado CE: '.$value);
            return $value;
        }
    }
}

// Funcion envio lead CRM
function sendLead(){

//        echo ('Ingreso Lead ');

        $nombres			=	$_POST['data']['nombre'];
        $apellidos			=	$_POST['data']['apellidos'];
        $numero 			=	$_POST['data']['numero'];
        $correo				=	$_POST['data']['correo'];
        $programa 			=	$_POST['data']['programa'];
        $identificacion 	=	$_POST['data']['identificacion'];

        $CEPrograma = CodigoExt($programa);

        $postData = "";
        #buscar en tabla api_clientes
        $send 	=	array(
            'user' 					=>	'landing_uvirtual', #cambiar usuario
            'password' 				=>	'zoomcrm', #contraseña en hash
            'nombres' 				=>	$nombres,
            'apellidos' 			=>	$apellidos,
            'celular' 				=>	$numero,
            'documento' 			=>	$identificacion,
            'email' 				=>	$correo,
            'productoServicio' 		=>	$CEPrograma,
            'medioVenta' 			=>	'DIGRS',
            'detalleMedioVenta' 	=>	'DIGRSFACE',
            'detalleFuenteVenta'	=>	'LANDIGGOOGLE'
        );

        foreach($send as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }

        $postData = rtrim($postData, '&');
        /*$ch = curl_init();
        #cambiar ruta https://test.zoomcrm.co/pi/home.php/api/wsrest/newlead/
        curl_setopt($ch, CURLOPT_URL,"https://test.zoomcrm.co/uvirtual/home.php/api/wsrest/newlead/");
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec ($ch);
        curl_close ($ch);

        $r =  json_decode($remote_server_output);
*/
//        print_r($send);

    $resSMS = sms($send);
    $send['resultadoSMS'] = $resSMS;
//    print_r($send);

//    insertRes($send);
}

// Funcion insertar resultado de envio DB
function insertRes($data){

    echo (' Ingreso BD');
    $table = 'leads';
    $precio = $data['resultadoSMS']['precio_envio'];

    if ($data['resultadoSMS']['resultado']==='0') {
        # code...
        $res 	= 'Enviado';
        $precio = '18';
    }else{
        $res = $data['resultadoSMS']['resultado_t'];
        $precio = '0';
    }

    $colum	=	array(
        'nombres'			=>	$data['nombres'],
        'apellidos'			=>	$data['apellidos'],
        'numero' 			=>	$data['celular'],
        'correo'			=>	$data['email'],
        'programa' 			=>	$data['productoServicio'],
        'identificacion' 	=>	$data['documento'],
        'precio' 			=>	$precio,
        'fecha_envio' 		=>	$data['resultadoSMS']['fecha_envio'],
        'resultadoSMS' 		=>	$res
    );

    $format = null;

//    global $wpdb;
//    $wpdb->insert($table, $colum, $format );

    /*
    --
    --	Table results sendings leads
    --
    CREATE TABLE `leads` (
      `id` int(15) NOT NULL PRIMARY KEY AUTO_INCREMENT,
      `nombres` varchar(100) NOT NULL,
      `apellidos` varchar(100) NOT NULL,
      `numero` varchar(20) NOT NULL,
      `correo` varchar(100) NOT NULL,
      `programa` varchar(100) NOT NULL,
      `identificacion` varchar(100) NOT NULL,
      `resultadoSMS` varchar(100) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    */
}

if(isset($_POST)) {
    if (!empty($_POST['data'])) {
        sendLead();
    }
}
