<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Global site tag (gtag.js) - Google Ads: 879434959 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-879434959"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-879434959');
    </script>

    <!-- Event snippet for PROGRAMAS VIRTUALES UVIRTUAL conversion page -->
    <script>
        gtag('event', 'conversion', {'send_to': 'AW-879434959/XMv5CO-R8JIBEM-5rKMD'});
    </script>

    <meta charset="utf-8"/>

    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Uvirtual - thank-you-page</title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- CSS Files -->
    <link href="assets/css/thank-page/material-kit.min.css?v=2.1.0" rel="stylesheet"/>
    <link href="assets/css/material-kit.css" rel="stylesheet"/>
    <link href="assets/css/whatsapp/whatsapp.css" rel="stylesheet"/>

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/thank-page/demo.css" rel="stylesheet"/>
    <link href="assets/css/thank-page/vertical-nav.css" rel="stylesheet"/>

    <style type="text/css">
        body {
            background-image: url('assets/img/logos/THANK-YOU-PAGE.jpg');
            background-size: cover;
            background-position: 30%;
            background-repeat: no-repeat;
        }

        @media (min-width: 545px) {
            body {
                background-image: url('assets/img/logos/THANK-YOU-PAGE.jpg');
                background-size: cover;
                background-position: 30%;
                background-repeat: no-repeat;
            }

             .img-fluid {
                max-width: 75%!important;
            }
        }

    </style>

</head>

<body>

<!--<div class="page-header" >
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="assets/img/logos/logo_uvirtual.png" alt="Circle Image" class="rounded-circle img-fluid first-logo logo-page logo-image">
              </div>
          <div class="col-md-8">

                  <h1 style="color: #390A55; font-family: 'Source Sans Pro', sans-serif; font-style: italic; font-weight: 600; text-align: center;">¡Gracias por registrarte!</h1><br>
                  <p style="color: #000; font-family: 'Open Sans', sans-serif; text-align: center; font-size:  18px;">En menos de 24 horas te contactaremos para brindarte toda la información que requieras y apoyarte en tu proyecto de vida, estas a un paso de ser profesional.<br><br>

                Todos los días somos más en nuestra comunidad y tu ya haces parte de la Uvirtual ¡Contigo, donde quiera que estés!</p>

          </div>
        </div>
    </div>
</div>-->

<div class="container-fluid body_body">
    <div class="container">

        <div class="row" style="height: 88vh;">

            <div class="col-lg-3 col-sm-12">
                <img class="img-fluid first-logo2 logo-page logo-image" style="margin-left: auto" src="assets/img/logos/logo_uvirtual.png"
                     >
            </div>
            <div class="col-lg-3 col-sm-8"></div>
            <div class="col-lg-6 col-lg-offset-2 col-sm-12">
                <h3 id="thanks">&iexcl;GRACIAS POR REGISTRARTE!</h3><br>
                <p id="block-thanks">En menos de 24 horas te contactaremos para brindarte toda la informaci&oacute;n que
                    requieras y apoyarte en tu proyecto de vida,
                </p>
                <p id="block-thanks" style="text-align: center !important;">
                    estas a un paso de ser profesional.
                </p>
                <br>
                <p id="block-thanks" style="text-align: center !important;">
                    Todos los d&iacute;as somos m&aacute;s en nuestra comunidad y t&uacute; ya haces
                </p>
                <p id="block-thanks" style="text-align: center !important;">
                    parte de la UVirtual &iexcl;Contigo, donde quiera que est&eacute;s!
                </p>
                <div class="custom-whatsapp margin-lf-rt-30 mt_20">
                    <a target="_blank" href="https://web.whatsapp.com/send?l=es&amp;phone=573505649166‬">
                        <i class="whatsapp-icon"></i>
                        <span class="leter-white" style="font-size: 18px">
                            Comun&iacute;cate con nosotros
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <footer>
            <!--<div class="col-md-3"></div>-->
            <div class="col-md-12">
                <p id="block-thanks-footer">Personer&iacute;a Jur&iacute;dica No. 4788 de 2012. SNIES 9915.<br>
                    Instituci&oacute;n de Educaci&oacute;n Superior sujeta a inspecci&oacute;n y vigilancia por el Ministerio de Educaci&oacute;n
                    Nacional.
                </p>
            </div>
        </footer>
    </div>
</div>
</body>

</html>
