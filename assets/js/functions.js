$(function () {
    $('#form').validate({
        errorClass: "my-error-class",
        rules: {
            nombre:{
                required: true,
            },
            apellidos:{
                required: true,
            },
            identificacion:{
                required: true,
                number: true,
                minlength:7
            },
            correo: {
                required: true,
                email: true
            },
            celular: {
                required: true,
                number: true,
                minlength:7,
                maxlength:10
            },
            terminos:{
                required: true,
            },
            programa:{
                required: true,
            }
        },
        messages: {
            nombre:{
                required: 'Por favor ingrese su nombre',
            },
            apellidos:{
                required: 'Por favor ingrese su apellido',
            },
            identificacion:{
                required: 'Por favor ingrese su numero de identificación',
                number: 'Por favor digite solo numeros',
                minlength: jQuery.validator.format("Su identificación no debe ser menor a {0} caracteres!"),
            },
            correo: {
                required: "Por favor ingresa tu email",
                email: "Ingresa un email válido"
            },
            celular: {
                required: "Por favor ingresa tu numero de celular",
                number: 'Por favor digite solo numeros',
                email: "Ingresa un numero válido",
                minlength: jQuery.validator.format("El numero telefonico no debe ser menor a {0} caracteres!"),
                maxlength: jQuery.validator.format("El numero telefonico no debe ser mayor a {0} caracteres!")
            },
            terminos:{
                required: 'Debe de aceptar los terminos y conciones',
            },
            programa:{
                required: 'Debe seleccionar al menos un programa',
            }
        },
    });
});

$('#registrar').on('click', function () {
    // event.preventDefault();
    var path = 'src/functions.php';

    var nombre         = $('#nombre').val();
    var numero         = $('#celular').val();
    var apellido       = $('#apellidos').val();
    var identificacion = $('#identificacion').val();
    var correo         = $('#correo').val();
    var programa       = $('#programa').val();
    // var ciudad         = $('#ciudad').val();

    if (nombre.length == 0 || numero.length == 0 || apellido.length == 0 ||
        identificacion.length == 0 || correo.length == 0 || programa.length == 0){
        return null;
    }

    var data = {
        'nombre': nombre,
        'numero': numero,
        'apellidos': apellido,
        'correo': correo,
        'programa': programa,
        'identificacion': identificacion
    };

    var $inHiddens = document.querySelectorAll('form input[type=hidden]');

    for (var i = 0, $inHidden; $inHidden = $inHiddens[i++];) {
        if ($inHidden.value != "") {
            data[$inHidden.id] = $inHidden.value;
        }
    }

    $.post(path, data).done(function (response) {
        console.log(response);
    });
});

$(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 670) {
            $("#pestana").fadeOut();
        }
        else {
            $("#pestana").fadeIn();
        }
    });
    getParametersUrl();
});

function getParametersUrl() {
    var url = window.location.search;
    var $form = $('form');
    if (url) {
        var data = url.substring(1);
        if (data) {
            var variables = data.split('&');
            for (var i = 0; i < variables.length; i++) {
                var variable = variables[i].split('=');
                var input = '<input type="hidden" id="' + variable[0] + '" name="' + variable[0] + '" value="' + variable[1] + '">';
                $form.append(input);
            }
        }
    }
}





